import requests, shutil

BASE_URL = 'http://127.0.0.1:8080'
STATUS_OK = requests.codes['ok']
STATUS_BAD_REQUEST = requests.codes['bad_request']

BUCKET_NAME = "requests"
OBJECT_NAME = "object"
BUCKET_URL = BASE_URL + "/" + BUCKET_NAME
OBJECT_URL = BUCKET_URL + "/" + OBJECT_NAME

def test_create_bucket():
    """POST /{bucketName = requests} should have status_code 200"""
    resp = requests.post(BASE_URL + '/' + BUCKET_NAME + '?create')
    assert resp.status_code == STATUS_OK

def test_list_bucket_objects():
    """GET /{bucketName = requests}?list should have status_code 200 and json's objects should be list"""
    resp = requests.get(BASE_URL + "/" + BUCKET_NAME + "?list")
    resp_json = resp.json()
    assert resp.status_code == STATUS_OK and isinstance(resp_json["objects"], list)

def test_create_object_successful():
    """POST /{bucketName}/{objectName} should have status_code 200"""
    resp = requests.post(OBJECT_URL + '?create')
    assert resp.status_code == STATUS_OK

def test_create_object_name_alphanumeric():
    """POST /{bucketName}/{objectName} should have status_code 200"""
    object_name = "test_1.txt"
    resp = requests.post(BUCKET_URL + "/" + object_name + '?create')
    assert resp.status_code == STATUS_OK

def test_create_object_invalid_bucket():
    """POST /{bucketName}/{objectName} should have status_code 400"""
    bucket_name = "invalid"
    object_name = "valid"
    resp = requests.post(BASE_URL + '/' + bucket_name + "/" + object_name + '?create')
    assert resp.status_code == STATUS_BAD_REQUEST

def test_create_object_invalid_object_name():
    """POST /{bucketName}/{objectName} should have status_code 400"""
    object_name = ".invalid"
    resp = requests.post(BUCKET_URL + "/" + object_name + '?create')
    assert resp.status_code == STATUS_BAD_REQUEST

def test_create_object_invalid_object_name2():
    """POST /{bucketName}/{objectName} should have status_code 400"""
    object_name = "invalid."
    resp = requests.post(OBJECT_URL + '?create')
    assert resp.status_code == STATUS_BAD_REQUEST

def test_create_object_duplicate_object_name():
    """POST /{bucketName}/{objectName} should have status_code 400"""
    resp = requests.post(OBJECT_URL + '?create')
    assert resp.status_code == STATUS_BAD_REQUEST

# def test_upload_all_part_successful():
#     """PUT /{bucketName}/{objectName} should have status_code 200"""
#     headers = {
#         "content-length": '103838544',
#         "content-md5": "b4513f0c94d98b6d26d48bde126a6eac"
#     }
#     files = {'file': open('test_img.jpg', 'rb')}
#     partNumber = '1'
#     url = OBJECT_URL + '?partNumber=' + partNumber
#     resp = requests.put(url, headers=headers, files=files)
#     assert resp.status_code == STATUS_OK

# def test_upload_object_part():
#     """POST /{bucketName}/{objectName} should have status_code 200"""
#     # headers = {"Content-MD5": "b7e896d53d2b6087c09a0f83f37e33c6"}
#     # resp = requests.put(OBJECT_URL + "?partNumber=" + 1, headers=headers)
#     # assert resp.status_code == STATUS_OK
#     image_url = "https://upload.wikimedia.org/wikipedia/commons/6/6e/1_lijiang_old_town_2012.jpg"
#     r = requests.get(image_url, stream=True)
#     if r.status_code == 200:
#         with open('test_img.jpg', 'wb') as out_file:
#             shutil.copyfileobj(r.raw, out_file)
#         del r
#         resp = requests.post(OBJECT_URL + "?partNumber=1")