package io.muic.backend.simpleobjectstorage.service;

import io.muic.backend.simpleobjectstorage.entity.Bucket;
import io.muic.backend.simpleobjectstorage.entity.ObjectItem;
import io.muic.backend.simpleobjectstorage.entity.ObjectPart;
import io.muic.backend.simpleobjectstorage.repository.BucketRepository;
import io.muic.backend.simpleobjectstorage.repository.ObjectPartRepository;
import io.muic.backend.simpleobjectstorage.repository.ObjectRepository;
import io.muic.backend.simpleobjectstorage.utility.NameChecker;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.*;


@Service
public class BucketService {

    @Autowired
    private BucketRepository bucketRepository;

    @Autowired
    private ObjectRepository objectRepository;

    @Autowired
    private ObjectPartRepository objectPartRepository;

    @Autowired
    private NameChecker nameChecker;

    private static final String ROOT_DIR = "bucket";

    public Map<String, Object> createBucket(String bucketName) {
        if (!isAvailableBucketName(bucketName)) {
            return null;
        }
        long currentTime = new Date().getTime();
        Bucket bucket = new Bucket(bucketName, currentTime, currentTime);
        bucketRepository.save(bucket);
        File file = new File(ROOT_DIR, bucketName);
        file.mkdirs();

        Map<String, Object> ret = new HashMap<>();
        ret.put("name", bucketName);
        ret.put("created", currentTime);
        ret.put("modified", currentTime);
        return ret;
    }

    public boolean dropBucket(String bucketName) {
        Bucket bucket = bucketRepository.findByName(bucketName);
        try {
            if (bucket != null) {
                File bucketDir = new File(ROOT_DIR, bucketName);
                FileUtils.deleteDirectory(bucketDir);
                bucketRepository.delete(bucket);
                return true;
            }
            return false;
        }
        catch (IOException e) {
            return false;
        }
    }

    public Map<String, Object> listObjectsInBucket(String bucketName) {
        Bucket bucket = bucketRepository.findByName(bucketName);
        if (bucket != null) {
            List<ObjectItem> objectItems = objectRepository.findAllByBucket(bucket);
            Map<String, Object> ret = new HashMap<>();
            ret.put("created", bucket.getCreated());
            ret.put("modified", bucket.getModified());
            ret.put("name", bucket.getName());
            List<Object> objects = new ArrayList<>();
            for (ObjectItem objectItem : objectItems) {
                Map<String, Object> object = new HashMap<>();
                object.put("name", objectItem.getName());
                object.put("eTag", objectItem.geteTag());
                object.put("created", objectItem.getCreated());
                object.put("modified", objectItem.getModified());
                objects.add(object);
            }
            ret.put("objects", objects);
            return ret;
        }
        return null;
    }

    private boolean isAvailableBucketName(String bucketName) {
        return nameChecker.isValidBucketName(bucketName) && !bucketRepository.existsByName(bucketName);
    }
}
