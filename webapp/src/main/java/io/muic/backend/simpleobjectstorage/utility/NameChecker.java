package io.muic.backend.simpleobjectstorage.utility;

import org.springframework.stereotype.Service;
import java.util.regex.Pattern;

@Service
public class NameChecker {

    private static final Pattern BUCKET_NAME_REGEX = Pattern.compile("^[\\w-]+$");

    private static final Pattern OBJECT_NAME_REGEX = Pattern.compile("^(?![.])(?!.*[.]$)[\\w-.]+");

    public boolean isValidBucketName(String bucketName) {
        return BUCKET_NAME_REGEX.matcher(bucketName).matches();
    }

    public boolean isValidObjectName(String objectName) {
        return OBJECT_NAME_REGEX.matcher(objectName).matches();
    }
}
