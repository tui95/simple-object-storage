package io.muic.backend.simpleobjectstorage.service;

import io.muic.backend.simpleobjectstorage.entity.ObjectItem;
import io.muic.backend.simpleobjectstorage.entity.ObjectMetadata;
import io.muic.backend.simpleobjectstorage.repository.ObjectMetadataRepository;
import io.muic.backend.simpleobjectstorage.repository.ObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ObjectMetadataService {

    @Autowired
    private ObjectRepository objectRepository;

    @Autowired
    private ObjectMetadataRepository objectMetadataRepository;

    public boolean addOrUpdateMetadataByKey(String bucketName, String objectName, String key, String value) {
        ObjectItem objectItem = objectRepository.findObjectItemByBucketNameAndName(bucketName, objectName);
        if (objectItem != null) {
            ObjectMetadata objectMetadata = objectMetadataRepository.findObjectMetadataByObjectItemIdAndMetadataKey(
                                                objectItem.getId(), key);
            if (objectMetadata == null) {
                objectMetadata = new ObjectMetadata();
            }
            objectMetadata.setMetadataKey(key);
            objectMetadata.setMetadataValue(value);
            objectMetadata.setObjectItem(objectItem);
            objectMetadataRepository.save(objectMetadata);

            return true;
        }
        return false;
    }

    public boolean deleteMetadataByKey(String bucketName, String objectName, String key) {
        ObjectItem objectItem = objectRepository.findObjectItemByBucketNameAndName(bucketName, objectName);
        if (objectItem != null) {
            ObjectMetadata objectMetadata = objectMetadataRepository.findObjectMetadataByObjectItemIdAndMetadataKey(
                                                                    objectItem.getId(),key);
            if (objectMetadata != null) {
                objectMetadataRepository.delete(objectMetadata);
                return true;
            }
        }
        return false;
    }

    public Map<String, String> getObjectMetadataByKey(String bucketName, String objectName, String key) {
        ObjectItem objectItem = objectRepository.findObjectItemByBucketNameAndName(bucketName, objectName);
        if (objectItem != null) {
            ObjectMetadata objectMetadata = objectMetadataRepository.findObjectMetadataByObjectItemIdAndMetadataKey(
                                                                    objectItem.getId(),key);
            if (objectMetadata != null) {
                Map<String, String> map = new HashMap<>();
                map.put(objectMetadata.getMetadataKey(), objectMetadata.getMetadataValue());
                return map;
            }
        }
        return null;
    }

    public Map<String, String> getAllObjectMetadata(String bucketName, String object) {
        ObjectItem objectItem = objectRepository.findObjectItemByBucketNameAndName(bucketName, object);
        if (objectItem != null) {
            List<ObjectMetadata> allObjectMetadata = objectMetadataRepository.findAllByObjectItemId(objectItem.getId());
            Map<String, String> ret = new HashMap<>();
            allObjectMetadata.forEach(objectMetadata ->
                    ret.put(objectMetadata.getMetadataKey(), objectMetadata.getMetadataValue()));
            return ret;
        }
        return null;
    }
}
