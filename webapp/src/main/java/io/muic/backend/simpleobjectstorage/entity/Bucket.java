package io.muic.backend.simpleobjectstorage.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Bucket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private long created;

    private long modified;

    public Bucket() {}

    @OneToMany(mappedBy = "bucket", cascade = CascadeType.REMOVE)
    private Set<ObjectItem> objectItems = new HashSet<>();

    public Bucket(String name, long created, long modified) {
        this.name = name;
        this.created = created;
        this.modified = modified;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }
}
