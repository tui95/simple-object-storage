package io.muic.backend.simpleobjectstorage.controller;

import io.muic.backend.simpleobjectstorage.service.ObjectMetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/{bucketName}/{objectName}")
public class ObjectMetadataController {

    @Autowired
    private ObjectMetadataService objectMetadataService;

    @PutMapping(params = {"metadata","key"})
    public ResponseEntity<Void> addOrUpdateObjectMetadataByKey(@PathVariable("bucketName") String bucketName,
                                                         @PathVariable("objectName") String objectName,
                                                         @RequestParam("key") String key,
                                                         @RequestBody String value) {
        boolean isUpdateSuccessfully = objectMetadataService.addOrUpdateMetadataByKey(bucketName,
                                                                                        objectName,
                                                                                        key, value);
        if (isUpdateSuccessfully) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping(params = {"metadata","key"})
    public ResponseEntity<Void> deleteObjectMetadataByKey(@PathVariable("bucketName") String bucketName,
                                            @PathVariable("objectName") String objectName,
                                            @RequestParam("key") String key) {
        boolean isDeleted = objectMetadataService.deleteMetadataByKey(bucketName, objectName, key);
        if (isDeleted) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping(params = {"metadata","key"})
    public ResponseEntity<Map<String, String>> getObjectMetadataByKey(@PathVariable("bucketName") String bucketName,
                                                                      @PathVariable("objectName") String objectName,
                                                                      @RequestParam("key") String key) {
        Map<String, String> objectMetadata = objectMetadataService.getObjectMetadataByKey(bucketName, objectName, key);
        if (objectMetadata != null) {
            return ResponseEntity.ok(objectMetadata);
        }
        return ResponseEntity.notFound().build();
    }


    @GetMapping(params = "metadata")
    public ResponseEntity<Map<String, String>> getAllObjectMetadata(@PathVariable("bucketName") String bucketName,
                                                                    @PathVariable("objectName") String objectName) {
        Map<String, String> allObjectMetadata = objectMetadataService.getAllObjectMetadata(bucketName, objectName);
        if (allObjectMetadata != null) {
            return ResponseEntity.ok(allObjectMetadata);
        }
        return ResponseEntity.notFound().build();
    }
}
