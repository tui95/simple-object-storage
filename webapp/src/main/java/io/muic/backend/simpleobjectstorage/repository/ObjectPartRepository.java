package io.muic.backend.simpleobjectstorage.repository;

import io.muic.backend.simpleobjectstorage.entity.ObjectItem;
import io.muic.backend.simpleobjectstorage.entity.ObjectPart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObjectPartRepository extends JpaRepository<ObjectPart, Long> {

    List<ObjectPart> findAllByObjectItemIdOrderByPartNumber(long id);

    ObjectPart findByObjectItemAndPartNumber(ObjectItem objectItem, int partNumber);
}
