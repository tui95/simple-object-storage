package io.muic.backend.simpleobjectstorage.entity;

import javax.persistence.*;

@Entity
public class ObjectPart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int partNumber;

    private String partMd5;

    private int partSize;

    @ManyToOne
    @JoinColumn
    private ObjectItem objectItem;

    public ObjectPart() {}

    public ObjectPart(int partNumber, ObjectItem objectItem) {
        this.partNumber = partNumber;
        this.objectItem = objectItem;
    }

    public long getId() {
        return id;
    }

    public int getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(int partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartMd5() {
        return partMd5;
    }

    public void setPartMd5(String partMd5) {
        this.partMd5 = partMd5;
    }

    public ObjectItem getObjectItem() {
        return objectItem;
    }

    public void setObjectItem(ObjectItem objectItem) {
        this.objectItem = objectItem;
    }

    public int getPartSize() {
        return partSize;
    }

    public void setPartSize(int partSize) {
        this.partSize = partSize;
    }
}
