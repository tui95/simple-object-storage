package io.muic.backend.simpleobjectstorage.entity;

import javax.persistence.*;

@Entity
public class ObjectMetadata {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String metadataKey;

    private String metadataValue;

    @ManyToOne
    @JoinColumn
    private ObjectItem objectItem;

    public ObjectMetadata() {}

    public ObjectMetadata(String metadataKey, String metadataValue) {
        this.metadataKey = metadataKey;
        this.metadataValue = metadataValue;
    }

    public long getId() {
        return id;
    }

    public String getMetadataKey() {
        return metadataKey;
    }

    public void setMetadataKey(String metadataKey) {
        this.metadataKey = metadataKey;
    }

    public String getMetadataValue() {
        return metadataValue;
    }

    public void setMetadataValue(String metadataValue) {
        this.metadataValue = metadataValue;
    }

    public ObjectItem getObjectItem() {
        return objectItem;
    }

    public void setObjectItem(ObjectItem objectItem) {
        this.objectItem = objectItem;
    }
}
