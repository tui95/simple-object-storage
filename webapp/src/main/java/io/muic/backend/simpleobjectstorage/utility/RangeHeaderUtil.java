package io.muic.backend.simpleobjectstorage.utility;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class RangeHeaderUtil {

    private static final Pattern MULTIPART_RANGE_HEADER_REGEX = Pattern.compile("^bytes=\\d*-\\d*(,\\d*-\\d*)*$");

    private static final Pattern RANGE_HEADER_REGEX = Pattern.compile("^bytes=\\d+-\\d+$");

    public boolean isValidMultipartRangesFormat(String rangeHeader) {
        return MULTIPART_RANGE_HEADER_REGEX.matcher(rangeHeader.trim()).matches();
    }

    private boolean isValidRangeFormat(String rangeHeader) {
        return RANGE_HEADER_REGEX.matcher(rangeHeader.trim()).matches();
    }

    public List<MutablePair<Integer, Integer>> convertMultipartRangesStringToListOfRange(String rangeHeader) {
        String[] rangesStr = StringUtils.removeStart(rangeHeader.trim(),"bytes=").split(",");
        List<MutablePair<Integer, Integer>> ranges = new ArrayList<>();
        for (String rangeStr : rangesStr) {
            MutablePair<Integer, Integer> range = new MutablePair<>();
            if (StringUtils.startsWith(rangeStr, "-")) {
                String[] splittedRange = rangeStr.split("-");
                range.setLeft(-Integer.valueOf(splittedRange[1]));
            }
            else if (StringUtils.endsWith(rangeStr, "-")) {
                String[] splittedRange = rangeStr.split("-");
                range.setRight(Integer.valueOf(splittedRange[0])); // encode Range: bytes=100-
            }
            else {
                String[] splittedRange = rangeStr.split("-");
                range.setLeft(Integer.valueOf(splittedRange[0]));
                range.setRight(Integer.valueOf(splittedRange[1]));
            }
            ranges.add(range);
        }
        return ranges;
    }

    public Range<Integer, Integer> convertRangeStringToRange(String rangeHeader, int totalSize) {
        if (!isValidRangeFormat(rangeHeader)) {
            return null;
        }
        String[] rangeStr = StringUtils.removeStart(rangeHeader.trim(),"bytes=").split("-", -1);
        Integer start = Integer.valueOf(rangeStr[0]);
        Integer end = Integer.valueOf(rangeStr[1]);

        if (0 <= start && start < end && start < totalSize) {
            return new Range<>(start, end);
        }
        return new Range<>(start, end);
    }
}
