package io.muic.backend.simpleobjectstorage.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ObjectItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String eTag;

    private long created;

    private long modified;

    @ManyToOne
    @JoinColumn
    private Bucket bucket;

    @OneToMany(mappedBy = "objectItem", cascade = CascadeType.REMOVE)
    private Set<ObjectPart> objectParts = new HashSet<>();

    @OneToMany(mappedBy = "objectItem", cascade = CascadeType.REMOVE)
    private Set<ObjectMetadata> objectMetadata = new HashSet<>();

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String geteTag() {
        return eTag;
    }

    public void seteTag(String eTag) {
        this.eTag = eTag;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public Bucket getBucket() {
        return bucket;
    }

    public void setBucket(Bucket bucket) {
        this.bucket = bucket;
    }

    public Set<ObjectPart> getObjectParts() {
        return objectParts;
    }

    public void setObjectParts(Set<ObjectPart> objectParts) {
        this.objectParts = objectParts;
    }

    public Set<ObjectMetadata> getObjectMetadata() {
        return objectMetadata;
    }

    public void setObjectMetadata(Set<ObjectMetadata> objectMetadata) {
        this.objectMetadata = objectMetadata;
    }
}
