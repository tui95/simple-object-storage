package io.muic.backend.simpleobjectstorage.service;

import io.muic.backend.simpleobjectstorage.entity.Bucket;
import io.muic.backend.simpleobjectstorage.entity.ObjectItem;
import io.muic.backend.simpleobjectstorage.entity.ObjectPart;
import io.muic.backend.simpleobjectstorage.repository.BucketRepository;
import io.muic.backend.simpleobjectstorage.repository.ObjectPartRepository;
import io.muic.backend.simpleobjectstorage.repository.ObjectRepository;
import io.muic.backend.simpleobjectstorage.utility.MD5Util;
import io.muic.backend.simpleobjectstorage.utility.NameChecker;
import io.muic.backend.simpleobjectstorage.utility.Range;
import io.muic.backend.simpleobjectstorage.utility.RangeHeaderUtil;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

@Service
public class ObjectService {

    @Autowired
    private ObjectRepository objectRepository;

    @Autowired
    private BucketRepository bucketRepository;

    @Autowired
    private ObjectPartRepository objectPartRepository;

    @Autowired
    private NameChecker nameChecker;

    @Autowired
    private MD5Util md5Util;

    @Autowired
    private RangeHeaderUtil rangeHeaderUtil;

    private static final String ROOT_DIR = "bucket";

    public ObjectItem createObject(String bucketName, String objectName) {
        if (isAvailableObjectName(bucketName, objectName)) {
            long currentTime = new Date().getTime();
            ObjectItem object = new ObjectItem();
            Bucket bucket = bucketRepository.findByName(bucketName);
            object.setName(objectName);
            object.setCreated(currentTime);
            object.setModified(currentTime);
            object.setBucket(bucket);
            objectRepository.save(object);
            return object;
        }
        return null;
    }

    public Map<String, Object> completeMultipartUpload(String bucketName, String objectName) {
        Map<String, Object> ret = new HashMap<>();
        try {

            Bucket bucket = bucketRepository.findByName(bucketName);
            ObjectItem objectItem = objectRepository.findByBucketAndName(bucket, objectName);
            if (bucket == null) {
                throw new IllegalArgumentException("InvalidBucket");
            }
            if (objectItem == null) {
                throw new IllegalArgumentException("InvalidObjectName");
            }
            List<ObjectPart> objectParts = objectPartRepository.findAllByObjectItemIdOrderByPartNumber(objectItem.getId());

            String eTag;
            if (objectParts.size() == 1) {
                ObjectPart objectPart = objectParts.get(0);
                eTag = objectPart.getPartMd5();
                ret.put("length", objectPart.getPartSize());
            }
            else {
                ByteArrayOutputStream allFilesContent = new ByteArrayOutputStream();

                List<String> md5s = new ArrayList<>();

                for (ObjectPart objectPart : objectParts) {
                    File file = new File(ROOT_DIR, String.format("%s/%s#part%d",
                            bucket.getName(), objectItem.getName(), objectPart.getPartNumber()));
                    byte[] fileContent = FileUtils.readFileToByteArray(file);
                    allFilesContent.write(fileContent);
                    md5s.add(objectPart.getPartMd5());
                }

                eTag = md5Util.generateETag(md5s) + "-" + md5s.size();
                ret.put("length", allFilesContent.size());
            }

            long currentTime = new Date().getTime();
            objectItem.seteTag(eTag);
            objectItem.setModified(currentTime);
            objectRepository.save(objectItem);

            ret.put("eTag", eTag);
            ret.put("name", objectItem.getName());
            return ret;
        } catch (IOException | DecoderException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public boolean deleteObject(String bucketName, String objectName) {
        ObjectItem objectItem = objectRepository.findObjectItemByBucketNameAndName(bucketName, objectName);
        if (objectItem != null) {
            List<ObjectPart> objectParts = objectPartRepository.findAllByObjectItemIdOrderByPartNumber(objectItem.getId());
            for (ObjectPart objectPart : objectParts) {
                File objectPartFile = new File(ROOT_DIR, String.format("%s/%s#part%d",
                        objectItem.getBucket().getName(), objectItem.getName(), objectPart.getPartNumber()));
                objectPartFile.delete();
                objectPartRepository.delete(objectPart);
            }
            objectRepository.delete(objectItem);

            return true;
        }
        return false;
    }

    public boolean downloadObject(String bucketName, String objectName, String rangeHeader, HttpServletResponse response) {
        ObjectItem objectItem = objectRepository.findObjectItemByBucketNameAndName(bucketName, objectName);
        try {
            if (objectItem != null) {

                List<ObjectPart> objectParts = objectPartRepository.findAllByObjectItemIdOrderByPartNumber(
                                                                                                objectItem.getId());

                List<FileInputStream> listFileInputStream = new ArrayList<>();
                int totalFileSize = 0;
                for (ObjectPart objectPart : objectParts) {
                    String fileName = String.format("%s/%s/%s#part%d", ROOT_DIR, bucketName,
                                                                        objectName, objectPart.getPartNumber());
                    FileInputStream fileInputStream = new FileInputStream(fileName);
                    listFileInputStream.add(fileInputStream);
                    totalFileSize += objectPart.getPartSize();
                }
                InputStream allFileInputStream = new SequenceInputStream(Collections.enumeration(listFileInputStream));

                if (StringUtils.isEmpty(rangeHeader)) {
                    IOUtils.copyLarge(allFileInputStream, response.getOutputStream());
                }
                else {
                    Range<Integer, Integer> range = rangeHeaderUtil.convertRangeStringToRange(rangeHeader, totalFileSize);
                    if (range == null) {
                        throw new RuntimeException("InvalidRange");
                    }
                    if (range.getEnd() > totalFileSize) {
                        IOUtils.copyLarge(allFileInputStream, response.getOutputStream());
                    }
                    else {
                        IOUtils.copyLarge(allFileInputStream, response.getOutputStream(), range.getStart(), range.getEnd());
                    }
                }
                response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", objectName));
                return true;
            }
            return false;
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isAvailableObjectName(String bucketName, String objectName) {
        Bucket bucket = bucketRepository.findByName(bucketName);
        return nameChecker.isValidObjectName(objectName) && bucket != null
                && !objectRepository.existsByBucketAndName(bucket, objectName);
    }
}
