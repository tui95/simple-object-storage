package io.muic.backend.simpleobjectstorage.service;

import io.muic.backend.simpleobjectstorage.entity.Bucket;
import io.muic.backend.simpleobjectstorage.entity.ObjectItem;
import io.muic.backend.simpleobjectstorage.entity.ObjectPart;
import io.muic.backend.simpleobjectstorage.repository.BucketRepository;
import io.muic.backend.simpleobjectstorage.repository.ObjectPartRepository;
import io.muic.backend.simpleobjectstorage.repository.ObjectRepository;
import io.muic.backend.simpleobjectstorage.utility.MD5Util;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

@Service
public class ObjectPartService {

    @Autowired
    private MD5Util md5Util;

    @Autowired
    private BucketRepository bucketRepository;

    @Autowired
    private ObjectRepository objectRepository;

    @Autowired
    private ObjectPartRepository objectPartRepository;

    private static final String ROOT_DIR = "bucket";

    public Map<String, Object> uploadObjectPart(String bucketName,
                                                          String objectName,
                                                          Integer partNumber,
                                                          String partSizeStr,
                                                          String partMd5,
                                                          HttpServletRequest request) {
        try {
            ServletInputStream inputStream = request.getInputStream();
            byte[] fileContent = IOUtils.toByteArray(inputStream);
            String computedMd5 = md5Util.generateMD5(fileContent);
            Bucket bucket = bucketRepository.findByName(bucketName);
            ObjectItem object = objectRepository.findByBucketAndName(bucket, objectName);
            int partSize = Integer.parseInt(partSizeStr);
            if (partSize != request.getContentLength()) {
                throw new IllegalArgumentException("LengthMismatched");
            }
            if (!partMd5.equals(computedMd5)) {
                throw new IllegalArgumentException("MD5Mismatched");
            }
            if (!isValidPartNumber(partNumber)) {
                throw new IllegalArgumentException("InvalidPartNumber");
            }
            if (bucket == null) {
                throw new IllegalArgumentException("InvalidBucket");
            }
            if (object == null || object.geteTag() != null) { // eTag != null means object is already complete
                throw new IllegalArgumentException("InvalidObjectName");
            }
            ObjectPart objectPart = objectPartRepository.findByObjectItemAndPartNumber(object, partNumber);
            if (objectPart == null) {
                objectPart = new ObjectPart(partNumber, object);
            }
            objectPart.setPartMd5(computedMd5);
            objectPart.setPartSize(partSize);
            objectPartRepository.save(objectPart);

            File uploadFile = new File(ROOT_DIR,
                                String.format("%s/%s#part%d", bucket.getName(), object.getName(), partNumber));
            FileUtils.writeByteArrayToFile(uploadFile, fileContent);

            Map<String, Object> ret = new HashMap<>();
            ret.put("partNumber", partNumber);
            ret.put("md5", computedMd5);
            ret.put("length", request.getContentLength());

            return ret;
        }
        catch (SocketException | IllegalArgumentException e) {
            throw new IllegalArgumentException("LengthMismatched");
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public boolean deleteObjectPart(String bucketName, String objectName, int partNumber) {
        Bucket bucket = bucketRepository.findByName(bucketName);
        ObjectItem objectItem = objectRepository.findByBucketAndName(bucket, objectName);

        if (objectItem != null && objectItem.geteTag() == null && isValidPartNumber(partNumber)) {
            ObjectPart objectPart = objectPartRepository.findByObjectItemAndPartNumber(objectItem, partNumber);
            if (objectPart == null) {
                return true;
            }
            File deleteFile = new File(ROOT_DIR, String.format("%s/%s#part%d",
                    bucket.getName(), objectItem.getName(), objectPart.getPartNumber()));
            objectPartRepository.delete(objectPart);
            return deleteFile.delete();
        }
        return false;
    }

    private boolean isValidPartNumber(int partNumber) {
        return 1 <= partNumber && partNumber <= 10000;
    }
}
