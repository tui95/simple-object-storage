package io.muic.backend.simpleobjectstorage.utility;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MD5Util {

    public String generateMD5(byte[] fileContent) {
        return DigestUtils.md5Hex(fileContent);
    }

    public String generateETag(List<String> md5s) throws DecoderException {
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : md5s) {
            stringBuilder.append(s);
        }
        return DigestUtils.md5Hex(Hex.decodeHex(stringBuilder.toString()));

    }
}
