package io.muic.backend.simpleobjectstorage.utility;

public class Range<S, E> {

    private S start;

    private E end;

    public Range() {}

    public Range(S start, E end) {
        this.start = start;
        this.end = end;
    }

    public S getStart() {
        return start;
    }

    public void setStart(S start) {
        this.start = start;
    }

    public E getEnd() {
        return end;
    }

    public void setEnd(E end) {
        this.end = end;
    }
}
