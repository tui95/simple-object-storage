package io.muic.backend.simpleobjectstorage.controller;

import io.muic.backend.simpleobjectstorage.entity.ObjectItem;
import io.muic.backend.simpleobjectstorage.service.ObjectService;
import io.muic.backend.simpleobjectstorage.service.ObjectPartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/{bucketName}/{objectName}")
public class ObjectController {

    @Autowired
    private ObjectService objectService;

    @Autowired
    private ObjectPartService objectPartService;

    @PostMapping(params = "create")
    public ResponseEntity<Void> createMultipartUploadTicket(@PathVariable("bucketName") String bucketName,
                                                              @PathVariable("objectName") String objectName) {
        ObjectItem object = objectService.createObject(bucketName, objectName);
        if (object != null) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @PutMapping
    public ResponseEntity<Map<String, Object>> uploadAllParts(@PathVariable("bucketName") String bucketName,
                                                              @PathVariable("objectName") String objectName,
                                                              @RequestParam("partNumber") int partNumber,
                                                              @RequestHeader("Content-Length") String partSize,
                                                              @RequestHeader("Content-MD5") String partMd5,
                                                              HttpServletRequest request) {
        try {
            Map<String, Object> json = objectPartService.uploadObjectPart(bucketName,
                    objectName, partNumber, partSize, partMd5, request);
            return ResponseEntity.ok(json);
        }
        catch (IllegalArgumentException e) {
            Map<String, Object> json = new HashMap<>();
            json.put("partNumber", partNumber);
            json.put("md5", partMd5);
            json.put("length", request.getContentLength());
            json.put("error", e.getMessage());
            return ResponseEntity.badRequest().body(json);
        }
    }

    @PostMapping(params = "complete")
    public ResponseEntity<Map<String, Object>> completeMultipartUpload(@PathVariable("bucketName") String bucketName,
                                          @PathVariable("objectName") String objectName) {
        try {
            Map<String, Object> response = objectService.completeMultipartUpload(bucketName, objectName);
            return ResponseEntity.ok(response);
        }
        catch (IllegalArgumentException e) {
            Map<String, Object> response = new HashMap<>();
            response.put("error", e);
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteObjectPart(@PathVariable("bucketName") String bucketName,
                             @PathVariable("objectName") String objectName,
                             @RequestParam("partNumber") int partNumber) {
        boolean isDeleteSuccessfully = objectPartService.deleteObjectPart(bucketName, objectName, partNumber);
        if (isDeleteSuccessfully) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping(params = "delete")
    public ResponseEntity<Void> deleteObject(@PathVariable("bucketName") String bucketName,
                               @PathVariable("objectName") String objectName) {
        boolean isDeleteSuccessfully = objectService.deleteObject(bucketName, objectName);
        if (isDeleteSuccessfully) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping
    public ResponseEntity<Void> downloadObject(@PathVariable("bucketName") String bucketName,
                                               @PathVariable("objectName") String objectName,
                                               @RequestHeader(name = "Range", required = false) String range,
                                               HttpServletResponse response) {
        try {
            boolean downloadSuccessfully = objectService.downloadObject(bucketName, objectName, range, response);
            if (downloadSuccessfully) {
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.notFound().build();
        }
        catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE).build();
        }
    }

}
