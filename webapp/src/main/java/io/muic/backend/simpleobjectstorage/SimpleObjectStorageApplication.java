package io.muic.backend.simpleobjectstorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleObjectStorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleObjectStorageApplication.class, args);
    }
}
