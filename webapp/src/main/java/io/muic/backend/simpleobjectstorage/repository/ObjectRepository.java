package io.muic.backend.simpleobjectstorage.repository;

import io.muic.backend.simpleobjectstorage.entity.Bucket;
import io.muic.backend.simpleobjectstorage.entity.ObjectItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObjectRepository extends JpaRepository<ObjectItem, Long> {

    boolean existsByBucketAndName(Bucket bucket, String objectName);

    List<ObjectItem> findAllByBucket(Bucket bucket);

    ObjectItem findByBucketAndName(Bucket bucket, String objectName);

    ObjectItem findObjectItemByBucketNameAndName(String bucketName, String objectName);
}
