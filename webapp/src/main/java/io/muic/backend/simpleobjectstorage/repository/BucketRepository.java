package io.muic.backend.simpleobjectstorage.repository;

import io.muic.backend.simpleobjectstorage.entity.Bucket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BucketRepository extends JpaRepository<Bucket, Long> {

    boolean existsByName(String bucketName);

    Bucket findByName(String bucketName);
}
