package io.muic.backend.simpleobjectstorage.repository;

import io.muic.backend.simpleobjectstorage.entity.ObjectMetadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObjectMetadataRepository extends JpaRepository<ObjectMetadata, Long> {

    ObjectMetadata findObjectMetadataByObjectItemIdAndMetadataKey(long objectItemId, String key);

    List<ObjectMetadata> findAllByObjectItemId(long objectItemId);
}
