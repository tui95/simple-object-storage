package io.muic.backend.simpleobjectstorage.controller;

import io.muic.backend.simpleobjectstorage.service.BucketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/{bucketName}")
public class BucketController {

    @Autowired
    private BucketService bucketService;

    @PostMapping(params = "create")
    public ResponseEntity<Map<String, Object>> createBucket(@PathVariable("bucketName") String bucketName) {
        Map<String, Object> response = bucketService.createBucket(bucketName);
        if (response != null) {
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping(params = "delete")
    public ResponseEntity<Void> dropBucket(@PathVariable("bucketName") String bucketName) {
        if (bucketService.dropBucket(bucketName)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping(params = "list")
    public ResponseEntity<Map<String, Object>> listObjectsInBucket(@PathVariable("bucketName") String bucketName) {
        Map<String, Object> response = bucketService.listObjectsInBucket(bucketName);
        if (response != null) {
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.badRequest().build();
    }
}
